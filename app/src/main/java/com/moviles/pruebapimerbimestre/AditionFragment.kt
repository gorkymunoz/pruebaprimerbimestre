package com.moviles.pruebapimerbimestre


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import org.w3c.dom.Text
import kotlin.random.Random


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AditionFragment : Fragment() {

    var gameStarted = false
    var score = 0
    var timeLeft = 10
    var round = 0
    var result = 0
    var operador = ""
    internal lateinit var tvRound: TextView
    internal lateinit var etResult: EditText
    internal lateinit var tvScore: TextView
    internal lateinit var tvOperator: TextView
    internal lateinit var tvTimeLeft: TextView
    internal lateinit var secondNumberAdition: TextView
    internal lateinit var firstNumberAdition: TextView
    internal lateinit var btnSend: Button
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer //implementa el Timer.

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View=inflater.inflate(R.layout.fragment_adition, container, false)
        firstNumberAdition = view.findViewById<TextView>(R.id.firstAdition)
        secondNumberAdition = view.findViewById<TextView>(R.id.secondAdition)
        tvRound = view.findViewById<TextView>(R.id.tvRoundAdition)
        tvScore = view.findViewById<TextView>(R.id.tvScoreAdition)
        tvOperator = view.findViewById<TextView>(R.id.tvOperadorAdition)
        tvTimeLeft = view.findViewById<TextView>(R.id.tvTimeLeftAdition)
        etResult = view.findViewById(R.id.inputAdition)
        btnSend = view.findViewById(R.id.btnSendAdition)
        return view
    }

    override fun onDetach() {
        super.onDetach()
        countDownTimer.cancel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvRound.text = getString(R.string.round,round)
        tvScore.text =getString(R.string.score,score)
        tvTimeLeft.text =getString(R.string.timeLeft,timeLeft)
        operador = arguments?.getString("operador") ?: "+"
        tvOperator.setText(operador)
        generateRandoms(operador)
        startFirstGame()
        btnSend.setOnClickListener {
            verifyResult()
        }
    }

    fun generateRandoms(operador:String){
        var randomNumberFirst = 0
        var randomNumberSecond = 0
        if(operador.equals("-") || operador.equals("/")){
            randomNumberFirst = Random.nextInt(5,10)
            randomNumberSecond = Random.nextInt(1,5)
        }else{
            randomNumberFirst = Random.nextInt(10)
            randomNumberSecond = Random.nextInt(10)
        }

        firstNumberAdition.setText(randomNumberFirst.toString())
        secondNumberAdition.setText(randomNumberSecond.toString())

        when(operador){
            "+"-> result = randomNumberFirst + randomNumberSecond
            "-"-> result = randomNumberFirst - randomNumberSecond
            "*"-> result = randomNumberFirst * randomNumberSecond
            "/"-> result = randomNumberFirst / randomNumberSecond

        }

    }

    private fun restoreGame() {
        countDownTimer.cancel()
        timeLeft = 10
        tvScore.text = getString(R.string.score, score)
        round = round + 1
        tvRound.text = getString(R.string.round,round)
        if(round==5){
            endGame()
        }else{

            countDownTimer = object  : CountDownTimer(initialCountDown, countDownInterval){
                override fun onFinish() {
                    restoreGame()
                }

                override fun onTick(millisUntilFinished: Long) {
                    timeLeft = millisUntilFinished.toInt() / 1000
                    tvTimeLeft.text = getString(R.string.timeLeft, timeLeft)
                }

            }
            countDownTimer.start()
        }
    }

    private fun startFirstGame() {
        score = 0
        round = 0
        tvScore.text = getString(R.string.score, score)
        tvRound.text = getString(R.string.round,round)
        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {
                countDownTimer.cancel()
                restoreGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                tvTimeLeft.text = getString(R.string.timeLeft, timeLeft)
            }
        }
        countDownTimer.start()
    }

    fun verifyResult(){
        val numeroIngresado =  etResult.text.toString()
        if(numeroIngresado.length>0){
            if(numeroIngresado.toInt() == result){
                incrementScore()
                Toast.makeText(context,"Correct",Toast.LENGTH_SHORT).show()
                etResult.setText("")
            }else{
                Toast.makeText(context,"Incorrect",Toast.LENGTH_SHORT).show()
                etResult.setText("")

            }
                countDownTimer.cancel()
                generateRandoms(operador)
                restoreGame()

        }else{
            Toast.makeText(context,"Debe ingresar un valor antes de enviar a comprobar",Toast.LENGTH_SHORT).show()
        }
    }

    fun endGame(){
        countDownTimer.cancel()
        Toast.makeText(context,"The score is ${score}", Toast.LENGTH_LONG).show()
        startFirstGame()
    }

    fun incrementScore(){
        when(timeLeft){
            in 8..10 -> score = score +100
            in 5..7 -> score = score +50
            in 1..4 -> score = score + 10
        }
        tvScore.text = getString(R.string.score, score)
    }



}
