package com.moviles.pruebapimerbimestre


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class InicioFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater.inflate(R.layout.fragment_inicio, container, false)
        val btnAdition = view.findViewById<Button>(R.id.btnAdition)
        clickButton(btnAdition,R.id.idNavigationToAdition,"+")
        val btnSubstraction =view.findViewById<Button>(R.id.btnSubstraction)
        clickButton(btnSubstraction,R.id.idNavigationToAdition,"-")
        val btnMultiplication = view.findViewById<Button>(R.id.btnMultiplication)
        clickButton(btnMultiplication,R.id.idNavigationToAdition,"*")
        val btnDivision = view.findViewById<Button>(R.id.btnDivision)
        clickButton(btnDivision,R.id.idNavigationToAdition,"/")
        // Inflate the layout for this fragment
        return view
    }

    fun clickButton(button:Button, id:Int,operator:String){
        button.setOnClickListener {
            val bundle = bundleOf("operador" to operator)
            findNavController().navigate(id,bundle)
        }

    }

}
